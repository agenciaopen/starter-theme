<?php

/**
 * Register theme initial settings
*/
require_once('inc/theme-settings.php');

/**
 * Register required plugins
*/
require_once('inc/plugins/required.php');

/**
 * Register acf initial settings
*/
require_once('inc/theme-acf.php');


/**
 * Register Custom Navigation Walker
*/
require_once('inc/theme-menu.php');

/**
 * Enqueue global styles and scripts
*/
require_once('inc/theme-enqueue.php');


/**
 * Remove wp tags
*/
require_once('inc/remove-wp.php');

/**
* Custom login page
*/
require_once('inc/login-page.php');

/**
* Custom admin page
*/
require_once('inc/wp-admin.php');

/**
* Upload SVG files
*/
require_once('inc/svg-upload.php');

/**
* Remove emojis
*/
require_once('inc/remove-emojis.php');


/**
* Compress all html except for admin
* -----> CAREFUL USE <------
*/
if (!current_user_can('administrator') && !is_admin()) {
    require_once('inc/compress-html.php');
}

/**
* Remove Gutenberg
*/
require_once('inc/remove-gutenberg.php');




?>
