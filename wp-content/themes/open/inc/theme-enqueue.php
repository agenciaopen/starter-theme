<?php

function open_enqueue() {
	wp_enqueue_style( 'global-project', get_template_directory_uri() . '/assets/css/style.min.css', array(), rand(111,9999), 'all'  );
    wp_deregister_script('jquery');
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'bs4-project', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'),  '', 'all' );
    wp_enqueue_script( 'global-project-js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'),  rand(111,9999), 'all' );
}
add_action( 'wp_enqueue_scripts', 'open_enqueue' );